__author__ = 'Songful'

class ImageError(Exception):
    pass
class CoordinateError(ImageError):
    pass


class Image:
    def __int__(self, width, height, filename="", background="#FFFFFF"):
        self.filename = filename
        self.__background = background
        self.__data = {}
        self.__width = width
        self.__height = height
        self.__colors = {self.__background}
    @property
    def background(self):
        return self.__background
    @property
    def width(self):
        return self.__width
    @property
    def height(self):
        return self.__height
    @property
    def colors(self):
        return self.__colors

    def __getitem__(self, coordinate):
        assert len(coordinate) == 2, "coordinate should be a 2-tuple"
        if(not(0 <= coordinate[0] <self.width) or
            not(0<= coordinate[1] < self.height)):
            raise CoordinateError(str(coordinate))
        return self.__data.get(tuple(coordinate), self.__background)
    def __setitem__(self, coordinate, color):
        assert len(coordinate) == 2,  "coordinate should be a 2-tuple"
        if(not(0 <= coordinate[0] <self.width) or
               not(0<= coordinate[1] < self.height)):
            raise CoordinateError(str(coordinate))
        if color == self.__background:
            self.__data.pop(tuple(coordinate),None)
        else:
            self.__data[tuple(coordinate)] = color
            self.__colors.add(color)
