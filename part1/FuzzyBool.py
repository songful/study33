__author__ = 'Songful'
class FuzzyBool:
    def __init__(self, value = 0.0):
        self.__value = value if 0.0 <= value <= 1.0 else 0.0
    def __invert__(self): #not operation
        return FuzzyBool(1.0 - self.__value)
    def __and__(self, other): # and(&) operation
        return FuzzyBool(min(self.__value,other.__value))
    def __iand__(self, other): # &= operation
        self.__value = min(self.__value, other.__value)
        return self
    def __or__(self, other): # and(&) operation
        return FuzzyBool(max(self.__value,other.__value))
    def __ior__(self, other): # &= operation
        self.__value = max(self.__value, other.__value)
        return self
    def __repr__(self):
        return ("{0}({1})".format(self.__class__.__name__,self.__value))
    def __str__(self):
            return str(self.__value)
    def __bool__(self):
        return self.__value > 0.5
    def __int__(self):
        return round(self.__value)
    def __float__(self):
        return self.__value
    def __lt__(self, other):
        return self.__value < other.__value
    def __eq__(self, other):
        return self.__value == other.__value
    def __le__(self, other):
        return self.__value <= other.__value
    def __hash__(self):
        return hash(id(self))
    def __format__(self, *args, **kwargs):
        return format(self.__value,*args, **kwargs)

    @staticmethod
    def conjunction(*fuzzies): # '*' one or some same type arguments
        return FuzzyBool(min(float(x) for x in fuzzies))
    @staticmethod
    def disjunction(*fuzzies):
        """Returns the logical or of all the FuzzyBools

        >>> FuzzyBool.disjunction(FuzzyBool(0.5), FuzzyBool(0.75), 0.2, 0.1)
        FuzzyBool(0.75)
        """
        return FuzzyBool(max([float(x) for x in fuzzies]))


# f = FuzzyBool(.75)
# print(f)
