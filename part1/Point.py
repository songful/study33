__author__ = 'Songful'
import math

class Point:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y
    def distance_from_origin(self):
        return math.hypot(self.x, self.y)
    def __eq__(self, other):
        return self.x == other.x and self.y == other.y
    def __repr__(self):
        return "Point({0.x!r},{0.y!r})".format(self)
    def __str__(self):
        return "({0.x!r},{0.y!r})".format(self)


# foo = Point(1,1)
# print(foo.__str__())
# print(foo.distance_from_origin())
# print(foo.__repr__())
#
# foo1 = Point(1.2, 3.42)
# print(foo1.__str__())
# print(repr(foo))
# print(repr(foo1))
# print(str(foo1))

class Circle(Point):
    def __init__(self,radius, x = 0,y = 0):
        super.__init__(x,y)
        self.radius = radius
    def area(self):
        return math.pi*(self.radius**2)
    def edge_distance_from_origin(self):
        return abs(self.distance_from_origin() - self.radius)
    def circumference(self):
        return 2*math.pi*(self.radius**2)
    def __eq__(self, other):
        return self.radius == other.radius and super().__eq__(other)
    def __repr__(self):
        return "Circle({0.radius!r},{0.x!r},{0.y!r})".format(self)
    def __str__(self):
        return "({0.radius!r},{0.x!r},{0.y!r})".format(self)
