__author__ = 'Songful'

class FooClass(object):
    """
    my very first class: FooClass
    """
    version = 0.1 #class (data) attribute

    def __init__(self,name='John Doe'):
        """
        constructor
        """
        self.name = name #class instance (data) attribute
        print("Created a class instance for " + name)

    def showname(self):
        """
         display instance attribute and class namee
        """
        print("Your name is ",self.name)
        print("My name is ", self.__class__.__name__)

    def showver(self):
        """
         display class(static) attribute
        """
        print(self.version)
    def addMe2Me(self,x):
        """
         apply + operation to attribute
        """
        return x + x

#fool = FooClass()
#fool.showname()
#fool.showver()
#print(fool.addMe2Me('XXxx'))


import sys
#sys.stdout.write("Hello World!\n")
#print(sys.platform)

# print(sys.version)
#! /usr/bin/env python
print(1+2*4)