__author__ = 'Songful'
import os

fname = input("Enter filename: ")

try:
    fobj = open(fname,'r')
except IOError as e:
    print("**** file open error:\n\t",e)
else:
    for eachline in fobj:
        print(eachline)
    fobj.close()

