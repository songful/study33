from initdata import db
import pickle

for (key, record) in [('bob', db['bob']), ('tom', db['tom']), ('sue', db['sue'])]:
    recfile = open(key+'.pkl', 'wb')
    pickle.dump(record,recfile)
    recfile.close()

