"""
Save in-memory database object to a file with custom formatting;
assume 'endrec.', 'enddb.', and '=>' are not used in the data;
assume db is dict of dict;  warning: eval can be dangerous - it
runs strings as code;  could also eval() record dict all at once;
could also dbfile.write(key + '\n') vs print(key, file=dbfile);
"""

dbFileName = 'people_file'
ENDDB = 'enddb.'
ENDREC = 'endrec.'
RECSEP = '=>'

def storeDbase(db, dbFileName = dbFileName):
    "formatted dump of database to flat file"
    dbFile = open(dbFileName, 'w')
    for key in db :
        # dbFile.write(key+'\n')
        print(key, file = dbFile)
        for (name, value) in db[key].items():
            # dbFile.write(name + RECSEP + repr(value) +'\n')
            print(name + RECSEP + repr(value), file=dbFile)
        # dbFile.write(ENDREC +'\n')
        print(ENDREC, file = dbFile)
    # dbFile.write(ENDDB +'\n')
    print(ENDDB, file = dbFile)
    dbFile.close()

def loadDbase(dbFileName = dbFileName):
    "parse data to reconstruct database"
    dbFile = open(dbFileName)
    import sys
    sys.stdin = dbFile
    db={}
    key = input()
    while key != ENDDB:
        rec = {}
        field = input()
        while field != ENDREC:
            name,value = field.split(RECSEP)
            rec[name] = eval(value)
            field = input()
        db[key] = rec
        key = input()
    return db

if __name__ == '__main__':
    # from initdata import db
    # storeDbase(db)
    db = loadDbase()
    for key in db:
        print(key, '=>\n', db[key])

    # for line in open(dbFileName):
    #     print(line, end= '')
