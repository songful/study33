#initialize data to be stored in files, pickles, shelves

#records
bob = {'nam':'Bob Smith','age':42, 'pay':30000, 'job':'dev'}
sue = {'nam':'Sue Jones','age':45, 'pay':40000, 'job':'hdw'}
tom = {'nam':'Tom ',     'age':40, 'pay':0, 'job':'None'}

# database
db = {}
db['bob'] = bob
db['sue'] = sue
db['tom'] = tom

if __name__ =='__main__':
    for key in db:
        print(key, '=>\n', db[key])
