from person_start import Person

class Manager(Person):
    def __init__(self, name, age, pay):
        Person.__init__(self, name, age, pay, 'manager')

    def __str__(self):
        return '<%s => %s>' % (self.__class__.__name__, self.name)

    def giveRaise(self, percent, bonus=0.1):
        self.pay *= (1.0 + percent + bonus)


if __name__ == '__main__':
    tom = Manager(name= 'Tom Doe', age = 40, pay = 50000)
    print(tom.lastname())
    # tom.giveRaise(.20)
    # print(tom.pay)
    print(tom)