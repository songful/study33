from tkinter import *
from tkinter102 import MyGUI

# main app window
mainwin = Tk()
Label(mainwin, text=__name__).pack()

#popup window
popup = Toplevel()
Label(popup, text='Attach').pack()
MyGUI(popup).pack(side=RIGHT)   # attach my frame
mainwin.mainloop()