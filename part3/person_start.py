class Person:
    def __init__(self, name, age, pay=0, job=None):
        self.name = name
        self.age = age
        self.pay = pay
        self.job = job

    def lastname(self):
        return self.name.split()[-1]
    def giveRaise(self, percent):
        self.pay *= percent

if __name__ == '__main__':
    bob = Person('Bob Smith', 42, 30000, 'software')
    sue = Person('Sue Jones', 45, 40000, 'hardware')
    print(bob.name, sue.pay)

    # print(bob.name.split()[-1])
    # sue.pay *= 1.5
    print(bob.lastname())
    sue.giveRaise(1.1)
    print(sue.pay)